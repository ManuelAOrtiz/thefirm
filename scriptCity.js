var citiesWrapper = document.getElementById("citiesWrapper");
var citiesWarsaw = document.getElementById("citiesWarsaw");
var warsawImage = document.getElementById("warsawImage");
var warsawEmployees = document.getElementById("warsawEmployees");
var citiesNYC = document.getElementById("citiesNYC");
var nycImage = document.getElementById("nycImage")
var nycEmployees = document.getElementById("nycEmployees");
var citiesSofia = document.getElementById("citiesSofia");
var sofiaImage = document.getElementById("sofiaImage");
var sofiaEmployees = document.getElementById("sofiaEmployees")
var citiesPhiladelphia = document.getElementById("citiesPhiladelphia");
var philadelphiaImage = document.getElementById("philadelphiaImage");
var philadelphiaEmployees = document.getElementById("philadelphiaEmployees");
var citiesBerlin = document.getElementById("citiesBerlin");
var berlinImage = document.getElementById("berlinImage");
var berlinEmployees = document.getElementById("berlinEmployees");
var citiesBarcelona = document.getElementById("citiesBarcelona");
var barcelonaImage = document.getElementById("barcelonaImage");
var barcelonaEmployees = document.getElementById("barcelonaEmployees");

class Firm{
	constructor(){
		this.employees = [];
		this.budget= [];
		this.change= [];
		this.images = [];
		this.getEmployee();
	}
	getEmployee(){
		var that = this;
		$.ajax({url: "https://bitbucket.org/OggiDanailov/firm/raw/2df585250847781831c5ab8ab4a7fdff8f5ef8fc/finances.json",
			success: function(result){
				// console.log(result)
				var firm = JSON.parse(result);
				console.log(firm);
				let i = 0;
				for(i = 0; i<firm.budget.length;i++){
					that.budget.push(firm.budget[i])
				}
				for(i = 0; i<firm.employees.length;i++){
					that.employees.push(firm.employees[i]);
				}
					that.change.push(firm.change);
				for(i = 0; i<firm.images.length;i++){
					that.images.push(firm.images[i]);
				}
			}
		})
	}
}

var firm = new Firm();


function populateImage(){
	warsawImage.style.backgroundImage = "url('"+ firm.images[0].Warsaw+"')"	
	warsawImage.style.backgroundSize = "100% 100%";	

	nycImage.style.backgroundImage = "url('"+ firm.images[1].NYC+"')"
	nycImage.style.backgroundSize = "100% 100%";	
	sofiaImage.style.backgroundImage = "url('"+ firm.images[2].Sofia+"')"
	sofiaImage.style.backgroundSize = "100% 100%";	
	philadelphiaImage.style.backgroundImage = "url('"+ firm.images[3].Philadelphia+"')";
	philadelphiaImage.style.backgroundSize = "100% 100%";
	berlinImage.style.backgroundImage = "url('"+ firm.images[4].Berlin+"')"	
	berlinImage.style.backgroundSize = "100% 100%";
	barcelonaImage.style.backgroundImage = "url('"+ firm.images[5].Barcelona+"')"	
	barcelonaImage.style.backgroundSize = "100% 100%";
}
function populateEmployee(){
	for(let i =0; i<firm.employees.length; i++){
		if("Warsaw" == firm.employees[i].city){
			warsawEmployees.innerHTML += " "+firm.employees[i].fname+" "+firm.employees[i].lname;
 		}
 	}
 		for(let i =0; i<firm.employees.length; i++){
		if("NYC" == firm.employees[i].city){
			nycEmployees.innerHTML += " "+firm.employees[i].fname+" "+firm.employees[i].lname;
 		}
 	}
 		for(let i =0; i<firm.employees.length; i++){
		if("Sofia" == firm.employees[i].city){
			sofiaEmployees.innerHTML += " "+firm.employees[i].fname+" "+firm.employees[i].lname;
 		}
 	}
 	
 		for(let i =0; i<firm.employees.length; i++){
		if("Philadelphia" == firm.employees[i].city){
			philadelphiaEmployees.innerHTML += " "+firm.employees[i].fname+" "+firm.employees[i].lname;
 		}
 	}
 		for(let i =0; i<firm.employees.length; i++){
		if("Berlin" == firm.employees[i].city){
			berlinEmployees.innerHTML += " "+firm.employees[i].fname+" "+firm.employees[i].lname;
 		}
 	}
 		for(let i =0; i<firm.employees.length; i++){
		if("Barcelona" == firm.employees[i].city){
			barcelonaEmployees.innerHTML += " "+firm.employees[i].fname+" "+firm.employees[i].lname;
 		}
 	}
}
function searchFromCities(){
	for(let i =0; i<firm.employees.length; i++){
		if(cities.value == firm.employees[i].city){
			createBox(firm.employees[i])
 		}
 	}
}	



window.addEventListener('load', function(){
	setTimeout(function(){
		populateEmployee();
		populateImage();
	},100)

})