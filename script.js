var wrapper = document.getElementById("wrapper");
var search = document.getElementById("search");
var submit = document.getElementById("submit");
var clear = document.getElementById('clear')
var cities = document.getElementById("cities");
var exp = document.getElementById("exp");
var boxwrap = document.getElementById("boxWrap");
var chartWrapper = document.getElementById("chartWrapper");
var clearCounter = 0;
var dataNames = [];
var dataChange = [];
Change = [];
var headshotArr = ['photo-0.jpg','photo-1.jpg','photo-4.jpg','photo-5.jpg','photo-8.jpg','photo-13.jpg','photo-17.jpg','photo-20.jpg', 'photo-23.jpg','photo-24.jpg', 'photo-2.jpg','photo-24.jpg','photo-25.jpg','photo-3.jpg','photo-6.jpg','photo-9.jpg','photo-26.jpg','photo-10.jpg','photo-11.jpg','photo-12.jpg','photo-14.jpg','photo-15.jpg','photo-27.jpg','photo-28.jpg','photo-29.jpg','photo-30.jpg','photo-16.jpg','photo-32.jpg','photo-18.jpg','photo-19.jpg','photo-21.jpg','photo-22.jpg']


class Firm{
	constructor(){
		this.employees = [];
		this.budget= [];
		this.change= [];
		this.images = [];
		this.getEmployee();
	}
	getEmployee(){
		var that = this;
		$.ajax({url: "https://bitbucket.org/OggiDanailov/firm/raw/2df585250847781831c5ab8ab4a7fdff8f5ef8fc/finances.json",
			success: function(result){
				// console.log(result)
				var firm = JSON.parse(result);
				console.log(firm);
				let i = 0;
				for(i = 0; i<firm.budget.length;i++){
					that.budget.push(firm.budget[i])
				}
				for(i = 0; i<firm.employees.length;i++){
					that.employees.push(firm.employees[i]);
				}
					that.change.push(firm.change);
				for(i = 0; i<firm.images.length;i++){
					that.images.push(firm.images[i]);
				}
			}
		})
	}
}

var firm = new Firm();

function employeesSort(){
	for(i = 0; i < firm.employees.length; i++){
			createBox(firm.employees[i])
	}
}

function budget() {
	for (var i = 0; i < firm.budget.length; i++) {
		if(i == 0){
			var car = firm.budget[i].car + firm.budget[i].gas;
			console.log(car)
		}
		if (i == 1) {
			var plane = firm.budget[i].plane + firm.budget[i].airport
			console.log(plane)
		}
		if(i == 2){
			var stay = firm.budget[i].hotel + firm.budget[i].services
			console.log(stay)
		}
		if(i == 3){
			var food = firm.budget[i].restaurants + firm.budget[i].bars
			console.log(food)
		}
	}
	var total = car + plane + stay + food
	// console.log(total)
	return total
}

function budgetDisplay() {
	for (var i = 0; i < firm.budget.length; i++) {
		if(i == 0){
			var car = firm.budget[i].car + firm.budget[i].gas;
			console.log(car)
		}
		if (i == 1) {
			var plane = firm.budget[i].plane + firm.budget[i].airport
			console.log(plane)
		}
		if(i == 2){
			var stay = firm.budget[i].hotel + firm.budget[i].services
			console.log(stay)
		}
		if(i == 3){
			var food = firm.budget[i].restaurants + firm.budget[i].bars
			console.log(food)
		}
	}
	var total = 'Car Budget: ' + car + ", " + 'Plane Budget: ' + plane + ", " + 'Stay Budget: ' + stay + ", " + 'Food Budget: ' + food
	return total
}

function searchFromYears(){
	for (var i = 0; i<firm.employees.length; i++) {
		if (exp.value == firm.employees[i].experience){
			createBox(firm.employees[i])
		}
 	}
}

function searchFromName(){
	console.log(search.value)
	for(let i=0; i<firm.employees.length; i++){
		if(search.value.toUpperCase() == firm.employees[i].fname.toUpperCase() || search.value.toUpperCase() == firm.employees[i].lname.toUpperCase()){
			createBox(firm.employees[i])
		}
	}
}

function promotionChecker(fname){
	var x  = fname;
	var objKeys = Object.keys(firm.change[0])
	var objValues = Object.values(firm.change[0])
	var promotion = false;
	var budget = 810;
	x = x.split('')
	x[0] = x[0].toUpperCase()
	x= x.join('')
	//console.log(x)
	
	if (objKeys.includes(x)){
		var num = objKeys.indexOf(x);
		var change = objValues[num];
		dataNames.push(x);
		dataChange.push(change)
	}
	if(change>budget){
		promotion = true
		//console.log(num)
		//console.log(change)
	}

	if(promotion==false){
		return "No"
	}else{
		return "Yes"
	}
}

function searchFromCities(){
	for(let i =0; i<firm.employees.length; i++){
		if(cities.value == firm.employees[i].city){
			createBox(firm.employees[i])
 		}
 	}
}
function getImage(city){
	if(city=="Warsaw"){
		return firm.images[0].Warsaw;
	}else if(city=="NYC"){
		return firm.images[1].NYC;
	}else if(city=="Sofia"){
		return firm.images[2].Sofia;
	}else if(city=="Philadelphia"){
		return firm.images[3].Philadelphia;
	}else if(city=="Berlin"){
		return firm.images[4].Berlin;
	}else if(city=="Barcelona"){
		return firm.images[5].Barcelona;
	}
}

function createBox(employee) {
	var box = document.createElement('div');
	box.id= 'box';
	box.className = 'boxclass';
	document.getElementById('boxWrap').appendChild(box);
	box.style.border = '2px solid black';
	box.style.height = "170px";
	box.style.width = '100%';
	box.style.display = "grid"
	box.style.gridTemplateColumns = '1fr 1fr'
	box.style.margin = '1% auto'
	box.style.boxShadow = '1px 1px 1px black'

	var textBox  = document.createElement('div');
	box.appendChild(textBox);
	textBox.id= 'textBox';
	// textBox.style.border = '2px solid red';
	textBox.style.height = "100%";
	textBox.style.width = '100%';
	textBox.style.margin = '0 auto';
	textBox.style.display = "grid"
	textBox.style.gridTemplateRows = '1fr 1fr 1fr 1fr';
	var x  = employee.fname;
	var objKeys = Object.keys(firm.change[0])
	var objValues = Object.values(firm.change[0])
	var promotion = false;
	var budget = 810;
	x = x.split('')
	x[0] = x[0].toUpperCase()
	x= x.join('')
	var num = 0;
	
	if (objKeys.includes(x)){
		num = objKeys.indexOf(x);
	}

	var nameBox = document.createElement('div');

 //    nameBox.style.border = '2px solid yellow';
     nameBox.style.height = "100%";
     nameBox.style.width = '100%';
     nameBox.style.margin = '0 auto';
     var employeeName = document.createTextNode("Name: "+employee.fname +" "+ employee.lname);
     nameBox.appendChild(employeeName);

     var cityBox = document.createElement('div');

     //cityBox.style.border = '2px solid blue';
     cityBox.style.height = "100%";
     cityBox.style.width = '100%';
     cityBox.style.margin = '0 auto';
     var employeeCity = document.createTextNode("Location: "+employee.city);
     cityBox.appendChild(employeeCity);

     var years = document.createElement('div');

     // years.style.border = '2px solid purple';
     years.style.height = "100%";
     years.style.width = '100%';
     years.style.margin = '0 auto';
     var experience = document.createTextNode("Years of experience: "+employee.experience);
     years.appendChild(experience);

     var promo = document.createElement('div');

    // promo.style.border = '2px solid green';
     promo.style.height = "100%";
    promo.style.width = '100%';
    promo.style.margin = '0 auto';
    var promotion = document.createTextNode("Ready to Promote: "+promotionChecker(employee.fname));

    promo.appendChild(promotion);

    textBox.appendChild(nameBox);
    textBox.appendChild(years);
    textBox.appendChild(cityBox);
    textBox.appendChild(promo);



	var photoBox = document.createElement('div');
	box.appendChild(photoBox)
	photoBox.id = 'photoBox';
	photoBox.style.height = "100%";
	photoBox.style.width = '100%';
	photoBox.style.margin = '0 auto';
	photoBox.style.display = "grid"
	photoBox.style.gridTemplateRows = '1fr'
	photoBox.style.gridTemplateColumns = '2fr'
	photoBox.style.backgroundImage = "url('images/"+headshotArr[num]+"')";
	photoBox.style.backgroundSize = "100% 100%";
}


function searchfilter(){
	var foundSomeone = false;
	if(search.value==""&&cities.value!==""&&exp.value!==0){
		for(let i =0; i<firm.employees.length; i++){
			if(cities.value == firm.employees[i].city){
				if (exp.value == firm.employees[i].experience){
					createBox(firm.employees[i]);
					foundSomeone = true;
				}
	 		}
 		}
	}else if(search.value!==""&&cities.value!==""&&exp.value==0){
		for(let i=0; i<firm.employees.length; i++){
			if(search.value.toUpperCase() == firm.employees[i].fname.toUpperCase() || search.value.toUpperCase() == firm.employees[i].lname.toUpperCase()){
				if (exp.value == firm.employees[i].experience){
					createBox(firm.employees[i]);
					foundSomeone = true;
				}
			}
		}
	}else if(search.value!==""&&cities.value==""&&exp.value!==0){
       for(let i=0; i<firm.employees.length; i++){
           if(search.value.toUpperCase() == firm.employees[i].fname.toUpperCase() || search.value.toUpperCase() == firm.employees[i].lname.toUpperCase()){
               if (exp.value == firm.employees[i].experience){
                   createBox(firm.employees[i]);
                   foundSomeone = true;
               	}
            }
        }
    }
	if(foundSomeone == false){
		var failure = document.createElement("h1");
		failure.innerHTML = "Couldn't find any employees that fit your search parameters.";
		boxWrap.appendChild(failure);
	}
}
console.log(firm);
var colorArray = [
                'rgba(255, 99, 132)',
                'rgba(54, 162, 235)',
                'rgba(255, 206, 86)',
                'rgba(75, 192, 192)',
                'rgba(153, 102, 255)',
                'rgba(255, 159, 64)'
            ];
function makeChart(){
	dataNames.splice(0,0,"Avg")
	dataChange.splice(0,0,dataChange.reduce(function(a,b){return a+b}))
	var num = dataChange[0];
	num = num/(dataChange.length-1);
	dataChange.splice(0,1,num);
	var ctx = document.createElement("CANVAS");
	chartWrapper.appendChild(ctx)
	var myChart = new Chart(ctx,{
		type: "bar",
		data: {
			labels: dataNames,
			datasets: [{
				label: "How much they brought back.",
				data: dataChange,
				backgroundColor: (()=>{
					let bgColors = [];
                    for(let z = 0; z < dataNames.length; z++) {
                        bgColors.push(colorArray[z % colorArray.length]);
                    }
                    return bgColors;
				})(),
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: true,
					}
				}]
			}
		}
	})
}



submit.addEventListener('click', function(){
	boxWrap.innerHTML=" ";
	dataChange = [];
	dataNames = [];
	chartWrapper.innerHTML = " "
	if(search.value==""&&cities.value==""&&exp.value==0){
		employeesSort();
		makeChart()
	}else if(search.value!==""&&cities.value==""&&exp.value==0){
		searchFromName();
		makeChart()
	}else if(search.value==""&&cities.value!==""&&exp.value==0){
		searchFromCities();
		makeChart();
	}else if(search.value==""&&cities.value==""&&exp.value!==0){
		searchFromYears();
		makeChart();
	}else{
		searchfilter();
		makeChart();
	}
})

clear.addEventListener('click', function(){
	if(clearCounter==0){
		boxWrap.innerHTML = '';
		search.value = '';
		cities.value = '';
		exp.value = 0;
		dataChange = [];
		dataNames = [];
		clearCounter++
	}else{
		chartWrapper.innerHTML = " ";
		clearCounter = 0;
	}	
})
//to display on employee page

function displayAllEmployees(){
	for(i = 0; i < firm.employees.length; i++){
		//createBox(firm.employees[i])
	}
}